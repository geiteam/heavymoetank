﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtkBehehaviour : StateMachineBehaviour {

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<BraveGurerrier>().Health <= animator.GetComponent<BraveGurerrier>().maxHealth * 0.7)
        {
            animator.SetTrigger("damage");
            animator.GetComponent<BraveGurerrier>().ChangeState(new GuerrierIdleState());
            animator.GetComponent<BraveGurerrier>().GetComponent<BoxCollider2D>().offset = new Vector2(1.508529f, 0.00835216f);
            animator.GetComponent<BraveGurerrier>().GetComponent<BoxCollider2D>().size = new Vector2(6.874707f, 1.192616f);
        } 
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
