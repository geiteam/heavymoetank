﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnBehaivour : StateMachineBehaviour {

    float respawnTime;

    float deadTimer;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        respawnTime = 0;
        deadTimer = 3;

        if (animator.GetComponent<TankController>().tank.Life == 0)
        {
            animator.GetComponent<TankController>().IndicatingGameover();
            Time.timeScale = 0;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        respawnTime += Time.deltaTime;
        if(respawnTime >= deadTimer)
        {
            if(animator.GetComponent<TankController>().tank.Life > 0)
            {
                animator.GetComponent<TankController>().Respawn();
            }
        }
       
        animator.ResetTrigger("dead");
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
