﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingPoint : MonoBehaviour {

    public string thingToFloat;

    public float floatSpeed;

    public Text displayPoint;

    private float timer = 0.5f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        displayPoint.text = thingToFloat;
        transform.position = new Vector3(transform.position.x, transform.position.y + (floatSpeed * Time.deltaTime));
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
