﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ammo : MonoBehaviour
{

    [SerializeField]
    private float speed;

    private Rigidbody2D myRigidbody;

    private Animator myAnimator;

    private Vector3 direction;

    public int rocketDamage;

    // Use this for initialization
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        if (transform.GetComponent<Animator>() != null)
        {
            myAnimator = GetComponent<Animator>();
        }
    }

    void FixedUpdate()
    {
        myRigidbody.velocity = new Vector2(direction.x, direction.y).normalized * speed;
    }

    public void Initialize(Vector3 direction)
    {
        this.direction = direction;
    }

    private IEnumerator ExplosionAmmo()
    {
        myAnimator.SetTrigger("explode");
        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Enemy" || target.tag == "Boss")
        {
            if (!target.GetComponent<Character>().IsDead)
            {
                StartCoroutine(ExplosionAmmo());
            }
            else
            {
                Physics2D.IgnoreCollision(target.transform.GetComponent<Collider2D>(), transform.GetComponent<Collider2D>(), true);
            }
            
        }
        if(target.tag == "Ground")
        {
            StartCoroutine(ExplosionAmmo());
        }
        else if (target.tag == "EnemyMissle")
        {
            StartCoroutine(ExplosionAmmo());
        }
        else if (target.tag == "BossBomb")
        {
            StartCoroutine(ExplosionAmmo());
        }
    }
}
