﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMissle : MonoBehaviour
{

    private Rigidbody2D myBody;

    private Animator myAni;

    private Transform target;

    private float timer;

    private float duration = .75f;

    private Vector3 direction;

    private bool impact;

    private void Awake()
    {
        myBody = GetComponent<Rigidbody2D>();
        myAni = GetComponent<Animator>();
        target = GameObject.Find("Tank").transform;
        myBody.AddForce(transform.up * 200f);
    }

    //private void OnDisable()
    //{
    //    //impact = false;
    //    myBody.gravityScale = 0;
    //    //direction = target.position - transform.position;
    //}

    private void OnEnable()
    {
        timer = 0;
        impact = false;
        direction = target.position - transform.position;
    }


    // Use this for initialization
    void Start()
    {
        direction = target.position - transform.position;
    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        if (!impact)
        {
            if (timer >= duration)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 30));
                myBody.gravityScale = 1;
            }
            else
            {
                myBody.velocity = direction.normalized * 5f;
            }
        }
        else
        {
            myBody.velocity = Vector3.zero;
        }
     
    }
    public IEnumerator ExplosionAmmo()
    {
        myAni.SetTrigger("explode");
        timer = 0;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        yield return new WaitForSeconds(1f);
        
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Ground" || target.tag == "Player")
        {
            impact = true;
            StartCoroutine(ExplosionAmmo());
        }
        if(target.tag == "PlayerAmmo" || target.tag == "PlayerRocket")
        {
            impact = true;
            StartCoroutine(ExplosionAmmo());
        }
    }

}
