﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private Animator myAni;

    private int bounceCount;

    [SerializeField]
    private float speed;

    private bool isGrounded = false;

    private int hitCount = 2;

    public bool bounceLeft;

	// Use this for initialization
	void Start () {
        myAni = GetComponent<Animator>();
        bounceCount = 1;
	}
	
	// Update is called once per frame
	void Update () {
        if (bounceLeft)
        {
            Move();
        }
        else
        {
            if (isGrounded)
            {
                Move();
            }
        }
    }

    void Move()
    {
        if (bounceLeft)
        {
            transform.Translate(Vector2.left * (speed * Time.deltaTime));
        }
        else
        {
            transform.Translate(Vector2.right * (speed * Time.deltaTime));
        }
    }

    IEnumerator StartDestroyingObject()
    {
        myAni.SetTrigger("explode");
        yield return new WaitForSeconds(0.2f);
        bounceCount = 0;
        isGrounded = false;
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "PlayerAmmo" || target.tag == "PlayerRocket")
        {
            hitCount--;
            if (hitCount <= 0)
            {
                StartCoroutine(StartDestroyingObject());
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag.Contains("EnemyAmmo"))
        {
            Physics2D.IgnoreCollision(target.gameObject.GetComponent<BoxCollider2D>(), transform.GetComponent<BoxCollider2D>(), true);
        }
        if (target.gameObject.tag.Contains("Ground"))
        {
            isGrounded = true;
            bounceCount++;
            if(bounceCount == 3)
            {
                StartCoroutine(StartDestroyingObject());
            }
        }
        if (target.gameObject.tag.Contains("Player"))
        {
            StartCoroutine(StartDestroyingObject());
        }
        if(target.gameObject.tag == "lazer")
        {
            StartCoroutine(StartDestroyingObject());
        }
    }
}
