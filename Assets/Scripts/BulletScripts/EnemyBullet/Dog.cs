﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour {

    private Animator myAni;

    private Rigidbody2D myBody;

    private bool isGrounded = false;

    private int hitCount = 2;

    // Use this for initialization
    void Start()
    {
        myAni = GetComponent<Animator>();
        myBody = GetComponent<Rigidbody2D>();
        if (Random.Range(0,2) == 1)
        {
            var scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (isGrounded)
        {
            Move();
        }
	}


    void Move()
    {
        if (transform.localScale.x < 0)
        {
            myBody.velocity = new Vector2(-250f * Time.deltaTime, myBody.velocity.y);
        }
        else
        {
            myBody.velocity = new Vector2(250f * Time.deltaTime, myBody.velocity.y);
        }

        myAni.SetFloat("run", 2);
    }

    IEnumerator IndicatingDestroyingObject()
    {
        myAni.SetTrigger("explode");
        yield return new WaitForSeconds(0.2f);
        isGrounded = false;
        hitCount = 2;
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag.Contains("Ground"))
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player" || target.tag == "Edge")
        {
            myAni.SetTrigger("explode");
            StartCoroutine(IndicatingDestroyingObject());
        }
        if(target.tag == "PlayerAmmo" || target.tag == "PlayerRocket" || target.tag == "lazer")
        {
            hitCount--;
            if(hitCount <= 0)
            {
                StartCoroutine(IndicatingDestroyingObject());
            }
        }
    }

}
