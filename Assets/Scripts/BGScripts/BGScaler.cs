﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScaler : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if(gameObject.GetComponent<MeshRenderer>() != null)
        {
            MeshRenderer mr = GetComponent<MeshRenderer>();
            Vector3 tempScale = transform.localScale;
            float height = mr.bounds.size.y;
            float width = mr.bounds.size.x;

            float worldHeight = Camera.main.orthographicSize * 2f;
            float worldWidth = worldHeight * Screen.width / Screen.height;

            tempScale.y = worldHeight / height;
            tempScale.x = worldWidth / width;

            transform.localScale = tempScale;
        }else
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            Vector3 tempScale = transform.localScale;
            float height = sr.bounds.size.y;
            float width = sr.bounds.size.x;

            float worldHeight = Camera.main.orthographicSize * 2f;
            float worldWidth = worldHeight * Screen.width / Screen.height;

            tempScale.y = worldHeight / height;
            tempScale.x = worldWidth / width;

            transform.localScale = tempScale;
        }
        

        
    }

}
