﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameplayController1 : MonoBehaviour
{
    [SerializeField]
    private Image lifeImg;

    [SerializeField]
    public Text scoreTxt;

    [SerializeField]
    private Sprite oneLife;

    [SerializeField]
    private Sprite twoLifes;

    [SerializeField]
    private Sprite threeLife;

    [SerializeField]
    private Sprite fourLifes;

    [SerializeField]
    private Sprite defaultLife;

    [SerializeField]
    private GameObject[] enemySpawners;

    [SerializeField]
    private GameObject boss;

    [SerializeField]
    private int highscore;

    [SerializeField]
    public GameObject guidePanel = null;

    [SerializeField]
    public GameObject pausePanel;

    [SerializeField]
    public GameObject gameoverPanel;

    [SerializeField]
    public GameObject pauseBtn;

    private bool isSpawned = false;

    private AudioSource audioSource;

    [SerializeField]
    public GameObject completedLevelPanel;

    [SerializeField]
    public GameObject playerCanvas;

    private float timeToSpawnBoss;

    [SerializeField]
    private GameObject warning;

    [SerializeField]
    private GameObject round;

    [SerializeField]
    private GameObject exitOptionCanvas;

    private float timer;

    

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
        Cursor.visible = false;

        if (!guidePanel.activeInHierarchy)
        {
            round.SetActive(true);
            audioSource.Play();
        }

        if (PlayerPrefs.HasKey("Mode"))
        {
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                timeToSpawnBoss = Random.Range(70, 80);
            }
            else
            {
                timeToSpawnBoss = Random.Range(100 , 120);
            }
        }
        
        exitOptionCanvas.SetActive(false);
#if UNITY_ANDROID
        pauseBtn.SetActive(true);
#endif

    }

    private void SetGuidePanel()
    {
        if (guidePanel != null)
        {
            if (Time.timeScale == 0 && guidePanel.activeInHierarchy)
            {
                if (Input.anyKeyDown)
                {
                    guide = false;
                    round.SetActive(true);
                    audioSource.Play();
                }
            }
        }
    }

    private void Awake()
    {
        Time.timeScale = 1;
        if (!File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
        {
            if (guidePanel != null)
            {
                guide = true;
            }
        }
    }

    public static GameplayController1 Gameplay1
    {
        get
        {
            return FindObjectOfType<GameplayController1>();
        }
    }

    public int LevelCount
    {
        get
        {
            return PlayerPrefs.GetInt("LevelCount");

        }
        set
        {
            PlayerPrefs.SetInt("LevelCount", value);
        }
    }

    public int Highscore
    {
        get
        {
            return highscore;
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
        SetGuidePanel();

        timer += Time.deltaTime;

        CompletingLevel();
        StartCoroutine(IndicatingRound());

    }

    private bool pause;

    private bool guide;

    private void OnGUI()
    {
#if UNITY_STANDALONE
        if (guide)
        {
            guidePanel.SetActive(true);
            Time.timeScale = 0;
            Cursor.visible = true;
        }
        else
        {
            if (guidePanel != null)
            {
                guidePanel.SetActive(false);
                Time.timeScale = 1;
                Cursor.visible = false;


            }
            if (pause)
            {
                Time.timeScale = 0;
                Cursor.visible = true;
                pausePanel.SetActive(true);
            }
            else if (!pause)
            {
                Time.timeScale = 1;
                Cursor.visible = false;
                pausePanel.SetActive(false);
            }

            if (isComplete)
            {

                Cursor.visible = true;
                Time.timeScale = 0;
                if (TankController.Tank.tank.Point > GetHighScore())
                {
                    SetHighScore(TankController.Tank.tank.Point);
                }
                else
                {
                    SetHighScore(TankController.Tank.tank.Point);
                }
                completedLevelPanel.SetActive(true);
                Save();
                completedLevelPanel.transform.Find("ScoreTxtImg").GetComponentInChildren<Text>().text = scoreTxt.text;
            }
        }
#endif
#if UNITY_ANDROID
        if (isComplete)
        {

            Time.timeScale = 0;
            if (TankController.Tank.tank.Point > GetHighScore())
            {
                SetHighScore(TankController.Tank.tank.Point);
            }
            else
            {
                SetHighScore(TankController.Tank.tank.Point);
            }
            completedLevelPanel.SetActive(true);
            Save();
            completedLevelPanel.transform.Find("ScoreTxtImg").GetComponentInChildren<Text>().text = scoreTxt.text;
            pauseBtn.gameObject.SetActive(false);
        }
#endif
    }

    IEnumerator WaitingFor2Secs()
    {
        yield return new WaitForSeconds(2f);
    }

    void UpdateUI()
    {
        scoreTxt.text = "" + TankController.Tank.tank.Point;
#if UNITY_STANDALONE
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale == 0 && pausePanel.activeInHierarchy)
        {
            pause = false;
        }
#endif
        if (lifeImg != null)
        {
            if (TankController.Tank.tank.Life == 4)
            {
                lifeImg.sprite = fourLifes;
            }
            else if (TankController.Tank.tank.Life == 3)
            {
                lifeImg.sprite = threeLife;
            }
            else if (TankController.Tank.tank.Life == 2)
            {
                lifeImg.sprite = twoLifes;
            }
            else if (TankController.Tank.tank.Life == 1)
            {
                lifeImg.sprite = oneLife;
            }
            else
            {
                lifeImg.sprite = defaultLife;
            }
        }
    }



    public int GetHighScore()
    {
        return PlayerPrefs.GetInt("High Score");
    }

    public void SetHighScore(int score)
    {
        PlayerPrefs.SetInt("High Score", score);
    }


    void BossSpawner()
    {
        if (timer >= timeToSpawnBoss)
        {
            for (int i = 0; i < enemySpawners.Length; i++)
            {
                enemySpawners[i].gameObject.SetActive(false);
            }
            StartCoroutine(IndicatingWaring());
        }
        else if (Time.timeScale > 0)
        {
            for (int i = 0; i < enemySpawners.Length; i++)
            {
                enemySpawners[i].gameObject.SetActive(true);
            }
        }
    }

    IEnumerator IndicatingWaring()
    {
        if (!isSpawned)
        {
            warning.gameObject.SetActive(true);

            yield return new WaitForSeconds(2f);

            warning.gameObject.SetActive(false);

            boss.gameObject.SetActive(true);
        }
        isSpawned = true;
    }

    IEnumerator IndicatingRound()
    {
        if (round.activeInHierarchy && !guidePanel.activeInHierarchy)
        {

            yield return new WaitForSeconds(3f);
            round.SetActive(false);
            audioSource.playOnAwake = true;

        }
        BossSpawner();
    }

    private bool isComplete = false;

    void CompletingLevel()
    {
        if (boss.GetComponent<Character>().IsDead && boss.activeInHierarchy)
        {
            StartCoroutine(WaitingFor2Secs());

            isComplete = true;
        }
    }

    private SaveTank CreateSaveTankObject()
    {
        SaveTank save = new SaveTank();
        save.Tank = TankController.Tank.tank;
        return save;
    }

    private void Save()
    {
        SaveTank save = CreateSaveTankObject();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/PlayerTank.save");
        bf.Serialize(file, save);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/PlayerTank.save", FileMode.Open);

            SaveTank save = bf.Deserialize(file) as SaveTank;
            file.Close();

            TankController.Tank.tank = new Tank(save.Tank);
        }
    }

    public void ChangeToNextLevel(int level)
    {
        Time.timeScale = 1;
        completedLevelPanel.SetActive(false);
        LevelLoader.Instance.LoadingScene(level);
    }

    public void MutedBackgroundSound()
    {
        audioSource.Pause();
        pausePanel.transform.Find("MuteBtn").gameObject.SetActive(false);
    }

    public void UnMutedBackgroundSound()
    {
        audioSource.UnPause();
        pausePanel.transform.Find("MuteBtn").gameObject.SetActive(true);
    }

    public void PauseMisson()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        pauseBtn.gameObject.SetActive(false);
    }

    public void ResumeMission()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
#if UNITY_ANDROID
        pauseBtn.gameObject.SetActive(true);
#endif
    }

    public void RestartMission()
    {
        if (PlayerPrefs.HasKey("Mode"))
        {
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                TankController.Tank.tank.Life = 4;
            }
            else
            {
                TankController.Tank.tank.Life = 2;
            }
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }

    public void IndicatingYesOption()
    {
        exitOptionCanvas.SetActive(false);
        completedLevelPanel.SetActive(false);
        gameoverPanel.SetActive(false);
        LevelLoader.Instance.LoadingSceneWithString("Menu", pausePanel);
    }


    public void IndicatingNoOption()
    {
        Time.timeScale = 0;
        exitOptionCanvas.SetActive(false);
    }

    public void ReturnToMainMenu()
    {
        if (!exitOptionCanvas.activeInHierarchy)
        {
            exitOptionCanvas.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
