﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingManager : MonoBehaviour {

    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

    public Text fire, left, right;

    [SerializeField]
    private GameObject settingPanel;

    private struct Constrants
    {
        public static string leftMouse = "Lbl";
        public static string rightMouse = "Rbl";
    }

    private GameObject currentKey;

	//Initialization
	void Start () {
        keys.Add("Fire", (KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Fire","Mouse0")));
        keys.Add("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left", "A")));
        keys.Add("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "D")));

        fire.text = string.Equals(keys["Fire"].ToString(), "Mouse0")  ? Constrants.leftMouse : keys["Fire"].ToString();
        left.text = keys["Left"].ToString();
        right.text = keys["Right"].ToString();
    }
	
    private void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey || e.isMouse)
            {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
            }
        }
    }
    public void ChangeKey(GameObject clickedBtn)
    {
        currentKey = clickedBtn;
    }

    public void SaveKeys()
    {
        foreach (var key in keys)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }
        PlayerPrefs.Save();
        settingPanel.SetActive(false);
    }
}
