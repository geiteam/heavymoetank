﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankSpawner : MonoBehaviour
{
    private GameObject enemyTank;

    private GameObject ifvTank;

    // Use this for initialization
    void Start()
    {
        enemyTank = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "EnemyTank(Clone)");
        ifvTank = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "IFVTank(Clone)");
        StartCoroutine(EnemySpawning());
    }


    IEnumerator EnemySpawning()
    {
        if (enemyTank != null && ifvTank != null)
        {
            if (!enemyTank.activeSelf && !ifvTank.activeSelf)
            {
                if (enemyTank.activeSelf && !ifvTank.activeSelf)
                {
                    enemyTank.transform.position = transform.position;
                    enemyTank.SetActive(true);
                }
                else if (!enemyTank.activeSelf && ifvTank.activeSelf)

                {
                    ifvTank.transform.position = transform.position;
                    ifvTank.SetActive(true);
                }
                else
                {
                    if (Random.Range(0, 2) == 0)
                    {
                        enemyTank.transform.position = transform.position;
                        enemyTank.SetActive(true);
                    }
                    else
                    {
                        ifvTank.transform.position = transform.position;
                        ifvTank.SetActive(true);
                    }
                }
            }
        }
        yield return new WaitForSeconds(4f);
        StartCoroutine(EnemySpawning());
    }
}
