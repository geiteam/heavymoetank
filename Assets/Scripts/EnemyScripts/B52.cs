﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B52 : Enemy {

    [SerializeField]
    private GameObject bombPrefabs;

    private void Awake()
    {
        if (PlayerPrefs.GetString("Mode").Equals("Easy"))
        {
            numberOfBombs = 3;
        }
        else
        {
            numberOfBombs = 5;
        }
    }

    private int numberOfBombs;

    public override void Start()
    {
        base.Start();
    }

    public IEnumerator InitializeBomb()
    {
        yield return new WaitForSeconds(.3f);
        if (bombPrefabs != null)
        {
            for (int i = 0; i < numberOfBombs; i++)
            {
                var bomb = ObjectPooler.SharedInstance.GetPooledObject("EnemyBomb");
                bomb.transform.position = shootingPos.position;
                bomb.transform.rotation = Quaternion.identity;
                bomb.SetActive(true);
                yield return new WaitForSeconds(.6f);
            }
        }
    }

    private void OnEnable()
    {
        health = Random.Range(100, 200);
        point = 200;
        StartCoroutine(InitializeBomb());
    }

    public override void Move()
    {
        base.Move();
    }

    public override IEnumerator TakingDamage()
    {
        return base.TakingDamage();
    }

    void Update()
    {
        Move();
    }
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
