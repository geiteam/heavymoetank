﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank : Enemy {

    public override IEnumerator InitializeBullets()
    {
        if (gameObject.name.Contains("EnemyTank"))
        {
            var bullet = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Cannon(Clone)");
            bullet.transform.position = shootingPos.position;
            bullet.transform.rotation = Quaternion.identity;
            bullet.SetActive(true);
        }
        else
        {
            var bullet = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "IFVRocket(Clone)");
            bullet.transform.position = shootingPos.position;
            bullet.transform.rotation = Quaternion.identity;
            bullet.SetActive(true);
        }
        yield return null;
    }

    public override bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        point = 200;
        health = 200;
        ChangeState(new TankPatrolState());
    }

    // Update is called once per frame
    void Update () {
        if (Target == null)
        {
            if (GameObject.Find("Tank") != null)
            {
                Target = GameObject.Find("Tank");
            }
        }
        if (!IsDead)
        {
            currentState.Execute();
        }
    }

    public override void OnTriggerEnter2D(Collider2D target)
    {
        base.OnTriggerEnter2D(target);
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        if(target.gameObject.tag == "Player")
        {
            Physics2D.IgnoreCollision(target.gameObject.GetComponent<BoxCollider2D>(), transform.GetComponent<BoxCollider2D>(), true);
        }
    }

    private void OnEnable()
    {
        point = 200;
        health = 200;
        ChangeState(new TankPatrolState());
    }

    public override IEnumerator TakingDamage()
    {
        if (TankController.Tank.tank.RocketDamage > 0)
        {
            health -= TankController.Tank.tank.RocketDamage;
        }
        health -= TankController.Tank.tank.DealingDamage;
        if (!IsDead)
        {
            StartCoroutine(IndicatingTakingDamage());
        }
        else
        {
            TankController.Tank.tank.Point += point;
            if (this is Enemy)
            {
                UponDeath();
            }
            var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
            clone.transform.position = transform.position;
            clone.GetComponent<FloatingPoint>().thingToFloat = point.ToString();
            MyAnimator.SetTrigger("dead");
            StartCoroutine(IndicatingDeath());
        }
        yield return null;
    }

    public override IEnumerator IndicatingTakingDamage()
    {
        return base.IndicatingTakingDamage();
    }

    public override IEnumerator IndicatingDeath()
    {
        yield return new WaitForSeconds(.5f);
        gameObject.SetActive(false);
    }

    public override void RemovingTarget()
    {
        Target = null;
    }
}
