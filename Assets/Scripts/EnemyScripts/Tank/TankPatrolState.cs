﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankPatrolState : IEnemyState {

    private float patrolTimer;

    private float patrolDuration = .2f;

    private EnemyTank tank;

    public void Enter(Enemy enemy)
    {
        tank = (EnemyTank)enemy;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
        return;
    }

    public void Execute()
    {
        Patroling();
    }

    public void Exit()
    {
    }

    private void Patroling()
    {
        tank.Move();
        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolDuration)
        {
            tank.ChangeState(new TankAtkState());
        }
    }
}
