﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAtkState : IEnemyState {

    private float timer;

    private float duration = 3f;

    private EnemyTank tank;

    public void Enter(Enemy enemy)
    {
        tank = (EnemyTank)enemy;
        timer = 0;
        tank.MyAnimator.SetTrigger("attack");
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        timer += Time.deltaTime;
        if(timer >= duration)
        {
            if (!TankController.Tank.IsDead)
            {
                tank.MyAnimator.SetTrigger("attack");
                timer = 0;
            }
        }
    }

    public void Exit()
    {
        
    }

}
