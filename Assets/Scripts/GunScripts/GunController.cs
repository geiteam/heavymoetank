﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GunController : MonoBehaviour
{
    [SerializeField]
    public FixedJoystick joybutton;

    private static GunController gun;

    public AudioSource audioSource;

    private float rocketShotTimer;

    private float timeToShot = 1f;

    [SerializeField]
    private AudioClip shootingSound;

    [SerializeField]
    public GameObject bulletPrefab;

    public GameObject rocketPrefab;


    [SerializeField]
    public Transform topShootingPos;

    [SerializeField]
    public Transform centerShootingPos;

    [SerializeField]
    public Transform bottomShootingPos;

    [SerializeField]
    public Transform laserShoot;

    public Animator MyAnimator { get; set; }


    public static GunController Gun
    {
        get
        {
            if (gun == null)
            {
                gun = FindObjectOfType<GunController>();
            }
            return gun;
        }
    }

    private void ShootingRocket()
    {
#if UNITY_STANDALONE
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition - transform.position);
        difference.Normalize();
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg));
        Vector3 shootDirection;
        shootDirection = Input.mousePosition;
        shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        GameObject topRocket = ObjectPooler.SharedInstance.GetPooledObject("PlayerRocket");
        topRocket.transform.position = topShootingPos.position;
        topRocket.transform.rotation = rotation;
        topRocket.GetComponent<Ammo>().Initialize(shootDirection);
        topRocket.SetActive(true);
        GameObject bottmRocket = ObjectPooler.SharedInstance.GetPooledObject("PlayerRocket");
        bottmRocket.transform.position = bottomShootingPos.position;
        bottmRocket.transform.rotation = rotation;
        bottmRocket.GetComponent<Ammo>().Initialize(shootDirection);
        bottmRocket.SetActive(true);
        topShootingPos.rotation = rotation;
        bottomShootingPos.rotation = rotation;
#endif
#if UNITY_ANDROID
        //Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition - transform.position);
        //difference.Normalize();
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(joybutton.Vertical, joybutton.Horizontal) * Mathf.Rad2Deg));
        Vector3 shootDirection;
        shootDirection = joybutton.inputVector;
        //shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        GameObject topRocket = ObjectPooler.SharedInstance.GetPooledObject("PlayerRocket");
        topRocket.transform.position = topShootingPos.position;
        topRocket.transform.rotation = rotation;
        topRocket.GetComponent<Ammo>().Initialize(shootDirection);
        topRocket.SetActive(true);
        GameObject bottmRocket = ObjectPooler.SharedInstance.GetPooledObject("PlayerRocket");
        bottmRocket.transform.position = bottomShootingPos.position;
        bottmRocket.transform.rotation = rotation;
        bottmRocket.GetComponent<Ammo>().Initialize(shootDirection);
        bottmRocket.SetActive(true);
        topShootingPos.rotation = rotation;
        bottomShootingPos.rotation = rotation;
#endif

    }

    private void InitializeBullet(GameObject bullet, Transform shootingPos)
    {
#if UNITY_STANDALONE
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition - transform.position);
        difference.Normalize();
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg));
        Vector3 shootDirection;
        shootDirection = Input.mousePosition;
        shootDirection.z = 0.0f;
        shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        bullet.transform.rotation = rotation;
        bullet.GetComponent<Ammo>().Initialize(shootDirection);
        shootingPos.transform.rotation = rotation;
#endif
#if UNITY_ANDROID
        //Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition - transform.position);
        //difference.Normalize();
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(joybutton.Vertical, joybutton.Horizontal) * Mathf.Rad2Deg));
        Vector3 shootDirection;
        shootDirection = joybutton.inputVector;
        shootDirection.z = 0.0f;
        //shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
        bullet.transform.rotation = rotation;
        bullet.GetComponent<Ammo>().Initialize(shootDirection);
        shootingPos.transform.rotation = rotation;
#endif
    }

    public void ShootingBullet()
    {
        
        GameObject bullet = ObjectPooler.SharedInstance.GetPooledObject("PlayerAmmo", "Ammo");
        if (bullet != null)
        {
            bullet.transform.position = centerShootingPos.transform.position;
            MyAnimator.SetTrigger("shoot");
            audioSource.PlayOneShot(shootingSound);
            InitializeBullet(bullet, centerShootingPos);
            if (TankController.Tank.tank.LargeBullet)
            {
                bullet.transform.localScale = new Vector3(1.5f, 1.5f, 1);
            }
            bullet.SetActive(true);
        }
        if (TankController.Tank.tank.RocketLauncher)
        {
            if (rocketShotTimer >= timeToShot)
            {
                ShootingRocket();
                rocketShotTimer = 0;
            }
        }

        if (TankController.Tank.tank.ExtraShot)
        {
            topShootingPos.gameObject.SetActive(true);
            GameObject bullet1 = ObjectPooler.SharedInstance.GetPooledObject("PlayerAmmo", "Ammo");
            if (bullet1 != null)
            {
                bullet1.transform.position = topShootingPos.transform.position;
                InitializeBullet(bullet1, topShootingPos);
                if (TankController.Tank.tank.LargeBullet)
                {
                    bullet1.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                }
                bullet1.SetActive(true);
            }
            bottomShootingPos.gameObject.SetActive(true);
            GameObject bullet2 = ObjectPooler.SharedInstance.GetPooledObject("PlayerAmmo", "Ammo");
            if (bullet2 != null)
            {
                bullet2.transform.position = bottomShootingPos.transform.position;
                InitializeBullet(bullet2, bottomShootingPos);
                if (TankController.Tank.tank.LargeBullet)
                {
                    bullet2.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                }
                bullet2.SetActive(true);
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        MyAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
#if UNITY_STANDALONE
        if (!TankController.Tank.IsDead && Time.timeScale > 0)
        {
            //Vector2 moveVec = new Vector2(joybutton.Horizontal,
            //joybutton.Vertical);


            //Vector3 lookVec = new Vector3(joybutton.Horizontal,
            //joybutton.Vertical, 4000);

            //transform.rotation = Quaternion.LookRotation(lookVec, Vector3.back);
            //transform.Translate(moveVec * Time.deltaTime * 7f, Space.World);
            Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition - transform.position);
            difference.Normalize();
            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg));
            transform.rotation = rotation;
            if (TankController.Tank.tank.RocketLauncher)
            {
                rocketShotTimer += Time.deltaTime;
            }
            if (TankController.Tank.tank.Laser)
            {
                laserShoot.gameObject.SetActive(true);
                centerShootingPos.gameObject.SetActive(false);
                if (TankController.Tank.tank.ExtraShot)
                {
                    topShootingPos.gameObject.SetActive(false);

                    bottomShootingPos.gameObject.SetActive(false);
                }
            }
            else
            {
                centerShootingPos.gameObject.SetActive(true);
                laserShoot.gameObject.SetActive(false);
                if (TankController.Tank.tank.ExtraShot)
                {
                    topShootingPos.gameObject.SetActive(true);

                    bottomShootingPos.gameObject.SetActive(true);
                }
            }
        }
#endif

#if UNITY_ANDROID
        if (!TankController.Tank.IsDead && Time.timeScale > 0)
        {
            //Vector2 moveVec = new Vector2(joybutton.Horizontal,
            //joybutton.Vertical);


            //Vector3 lookVec = new Vector3(joybutton.Horizontal,
            //joybutton.Vertical, 4000);

            //transform.rotation = Quaternion.LookRotation(lookVec, Vector3.back);
            //transform.Translate(moveVec * Time.deltaTime * 7f, Space.World);
            //Vector3 difference = Camera.main.ScreenToWorldPoint(joybutton.transform.position - transform.position);
            //difference.Normalize();
            Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(joybutton.Vertical, joybutton.Horizontal) * Mathf.Rad2Deg));
            transform.rotation = rotation;
            //transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(joybutton., y) * Mathf.rad2deg, char.transform.eulerAngles.z);
            if (TankController.Tank.tank.RocketLauncher)
            {
                rocketShotTimer += Time.deltaTime;
            }
            if (TankController.Tank.tank.Laser)
            {
                laserShoot.gameObject.SetActive(true);
                centerShootingPos.gameObject.SetActive(false);
                if (TankController.Tank.tank.ExtraShot)
                {
                    topShootingPos.gameObject.SetActive(false);

                    bottomShootingPos.gameObject.SetActive(false);
                }
            }
            else
            {
                centerShootingPos.gameObject.SetActive(true);
                laserShoot.gameObject.SetActive(false);
                if (TankController.Tank.tank.ExtraShot)
                {
                    topShootingPos.gameObject.SetActive(true);

                    bottomShootingPos.gameObject.SetActive(true);
                }
            }
        }
#endif


    }
}
