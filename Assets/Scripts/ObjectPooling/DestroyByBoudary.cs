﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoudary : MonoBehaviour {

    private void OnTriggerExit2D(Collider2D collision)
    {
            if (collision.tag == "PlayerAmmo" || collision.tag == "EnemyAmmo" || collision.tag == "PlayerRocket")
            {
                collision.gameObject.SetActive(false);
            }
    }
}
