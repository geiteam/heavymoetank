﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuerrierIdleState : IEnemyState {

    private BraveGurerrier gurerrier;

    public void Enter(Enemy gurerrier)
    {
        this.gurerrier = (BraveGurerrier)gurerrier;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {

    }

    public void Execute()
    {
        if(Random.Range(0,2) == 1)
        {
            gurerrier.ChangeState(new GuerrierAtkState());
        }
    }

    public void Exit()
    {
    }

}
