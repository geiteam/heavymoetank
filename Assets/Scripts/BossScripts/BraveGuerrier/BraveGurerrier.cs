﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BraveGurerrier : Enemy
{
    [SerializeField]
    private Transform leftTurretAndLeftPos;

    [SerializeField]
    private Transform leftTurretAndRightPos;

    [SerializeField]
    private Transform rightTurretAndLeftPos;

    [SerializeField]
    private Transform rightTurretAndRightPos;

    private int numberOfMissles;

    [SerializeField]
    private Transform spawningDogPos;

    [SerializeField]
    private Transform spawningSoliderPos;

    [SerializeField]
    private Transform shootingMisslePos;

    private float timer;

    private float duration;

    [HideInInspector]
    public int maxHealth;

    [SerializeField]
    private GameObject bouncingBulletPrefab;

    [SerializeField]
    private GameObject misslePrefab;

    [SerializeField]
    private GameObject dogPrefab;

    [SerializeField]
    private GameObject soilderPrefab;


    public void ShootingBouncingBullet()
    {
        if (!TankController.Tank.IsDead)
        {
            if (timer >= duration)
            {
                if (Random.Range(0, 2) == 0)
                {
                    var leftBall = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Ball(Clone)");
                    leftBall.transform.position = leftTurretAndLeftPos.position;
                    leftBall.transform.rotation = Quaternion.identity;
                    leftBall.SetActive(true);
                    var rightBall = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Ball(Clone)");
                    rightBall.transform.position = leftTurretAndRightPos.position;
                    rightBall.transform.rotation = Quaternion.identity;
                    rightBall.SetActive(true);
                }
                else
                {
                    var leftBall = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Ball(Clone)");
                    leftBall.transform.position = rightTurretAndLeftPos.position;
                    leftBall.transform.rotation = Quaternion.identity;
                    leftBall.SetActive(true);
                    var rightBall = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Ball(Clone)");
                    rightBall.transform.position = rightTurretAndRightPos.position;
                    rightBall.transform.rotation = Quaternion.identity;
                    rightBall.SetActive(true);
                }
                timer = 0;
            }
        }
    }


    public void SpawningSoilderAndDog()
    {
        if (!TankController.Tank.IsDead)
        {
            if (health > maxHealth * 0.3)
            {
                if (!soilderPrefab.gameObject.activeInHierarchy)
                {
                    soilderPrefab.SetActive(true);
                    soilderPrefab.GetComponent<Soilder>().holder.isTrigger = false;
                    soilderPrefab.transform.position = spawningSoliderPos.position;
                }
            }
            if (health > maxHealth * 0.7)
            {
                if (PlayerPrefs.GetString("Mode").Equals("Easy"))
                {
                    if (GameObject.Find("Dog") == null)
                    {
                        var dog = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Dog");
                        dog.transform.position = spawningDogPos.position;
                        dog.transform.rotation = Quaternion.identity;
                        dog.SetActive(true);
                    }
                }
                else
                {
                    for(int i = 0; i < 2; i++)
                    {
                        if (GameObject.Find("Dog") == null)
                        {
                            var dog = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo", "Dog");
                            dog.transform.position = spawningDogPos.position;
                            dog.transform.rotation = Quaternion.identity;
                            dog.SetActive(true);
                            StartCoroutine(WaitForSecs());
                        }
                    }
                }
                
            }
        }
    }

    private IEnumerator WaitForSecs()
    {
        yield return new WaitForSeconds(.3f);
    }

    public override bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Mode"))
        {
            if (PlayerPrefs.HasKey("Mode"))
            {
                if (PlayerPrefs.GetString("Mode").Equals("Easy"))
                {
                    numShots = 1;
                    duration = 3;
                    numberOfMissles = 2;
                    
                }
                else
                {
                    numShots = 4;
                    duration = 1f; numberOfMissles = 4;
                }
            }
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        point = Random.Range(10000, 15000);
        TankController.Tank.DeadEventHandler += new EventHandler(RemovingTarget);
        ChangeState(new GuerrierIdleState());
        maxHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        if (Target == null)
        {
            if (GameObject.Find("Tank") != null)
            {
                Target = GameObject.FindGameObjectWithTag("Player");
            }
        }
        if (!IsDead)
        {
            if (Target != null)
            {
                timer += Time.deltaTime;
                currentState.Execute();
            }
        }
    }

    public override void OnTriggerEnter2D(Collider2D target)
    {
        if (damageSources.Contains(target.tag))
        {
            if (!IsDead)
            {
                StartCoroutine(TakingDamage());
            }
        }
    }

    public override IEnumerator InitializeBullets()
    {
        if (Target != null)
        {
            if (!TankController.Tank.IsDead)
            {
                float xDir = Target.transform.position.x - shootingPos.position.x;
                float yDir = Target.transform.position.y - shootingPos.position.y;
                for (int i = 0; i < bullets.Length; i++)
                {
                    bullets[i] = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo");
                    bullets[i].transform.position = shootingPos.position;
                    bullets[i].transform.rotation = Quaternion.identity;
                    bullets[i].GetComponent<EnemyBullet>().Initialize(new Vector3(xDir, yDir, 0));
                    bullets[i].SetActive(true);
                    yield return new WaitForSeconds(.3f);
                }
            }
        }
    }

    public IEnumerator InitializeMissles()
    {
        if (Target != null)
        {
            if (!TankController.Tank.IsDead)
            {
                for (int i = 0; i < numberOfMissles; i++)
                {
                    var missle = ObjectPooler.SharedInstance.GetPooledObject("EnemyMissle");
                    missle.transform.position = shootingMisslePos.position;
                    missle.transform.rotation = Quaternion.identity;
                    missle.SetActive(true);
                    if (!facingRight)
                    {
                        missle.GetComponent<HomingMissle>().ChangeDirection();
                    }
                    yield return new WaitForSeconds(.5f);
                }
            }
        }
    }

    public void ShootingMissles()
    {
        StartCoroutine(InitializeMissles());
    }

    public override IEnumerator TakingDamage()
    {
        if (TankController.Tank.tank.RocketDamage > 0)
        {
            health -= TankController.Tank.tank.RocketDamage;
        }
        health -= TankController.Tank.tank.DealingDamage;
        if (!IsDead)
        {
            StartCoroutine(IndicatingTakingDamage());
        }
        else
        {
            TankController.Tank.tank.Point += point;
            var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
            clone.transform.position = transform.position;
            clone.GetComponent<FloatingPoint>().thingToFloat = point.ToString();
            MyAnimator.SetTrigger("dead");
            StartCoroutine(IndicatingDeath());
        }
        yield return null;
    }

    //public override IEnumerator TakingDamageWithLaser(int laserDamage)
    //{
    //    health -= laserDamage;
    //    if (Time.deltaTime >= 0.2f)
    //    {
    //        health -= laserDamage * 2;
    //    }
    //    if (!IsDead)
    //    {
    //        StartCoroutine(IndicatingTakingDamage());
    //    }
    //    else
    //    {
    //        TankController.Tank.Point += point;
    //        if (this is Enemy)
    //        {
    //            UponDeath();
    //        }
    //        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
    //        clone.transform.position = transform.position;
    //        clone.GetComponent<FloatingPoint>().thingToFloat = point.ToString();
    //        MyAnimator.SetTrigger("dead");
    //        StartCoroutine(IndicatingDeath());
    //    }
    //    yield return null;
    //}

    public override IEnumerator IndicatingTakingDamage()
    {
        transform.GetComponent<SpriteRenderer>().color = new Color(255, 27, 27, 255);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = new Color(255, 60, 60, 255);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = currentColor;
    }

    public override void OnDeath()
    {
        base.OnDeath();
    }
}
