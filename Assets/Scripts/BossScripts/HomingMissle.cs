﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HomingMissle : MonoBehaviour {

    [SerializeField]
    private float speed;

    private Rigidbody2D myRigidbody;

    private float homingDuration = 2;

    private float timer;

    private int hitCount = 2;

    private Animator myAnimator;

    private Transform target;

    private Color currentColor;

    [SerializeField]
    private float rotateSpeed = 200f;

    private void Awake()
    {
        timer = 0;
        myAnimator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        target = GameObject.Find("Tank").transform;
        currentColor = GetComponent<SpriteRenderer>().color;
    }
    void FixedUpdate()
    {
        var lastPos = target.position;
        if(timer >= homingDuration)
        {
            myRigidbody.velocity = lastPos.normalized * 400f * Time.deltaTime;
        }
        else
        {
            if(target != null)
            {
                Vector3 point2Target = (Vector2)target.position - (Vector2)transform.position;

                point2Target.Normalize();

                float rotateAmount = Vector3.Cross(point2Target, transform.right).z;
                if (transform.localScale.x < 0)
                {
                    myRigidbody.angularVelocity = -rotateAmount * rotateSpeed;
                }
                else
                {
                    myRigidbody.angularVelocity = rotateAmount * rotateSpeed;
                }


                float xDir = target.transform.position.x - transform.position.x;
                float yDir = target.transform.position.y - transform.position.y;

                myRigidbody.velocity = new Vector3(xDir, yDir, 1).normalized * speed;
                lastPos = target.transform.position - transform.position;
            }
            else
            {
                StartCoroutine(StartExploding());
            }
            
        }
    }
    void Update()
    {
        timer += Time.deltaTime;
    }

    public virtual void ChangeDirection()
    {
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, 1);
    }

    public IEnumerator HittingMissle()
    {
        hitCount -= 1;
        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 220);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 200);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = currentColor;
        if (hitCount <= 0)
        {
            StartCoroutine(StartExploding());
        }
        yield return null;
    }

    public IEnumerator StartExploding()
    {
        if (myAnimator != null)
        {
            myAnimator.SetTrigger("explode");
            timer = 0;
            yield return new WaitForSeconds(0.1f);
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground" || collision.tag == "Player")
        {
            StartCoroutine(StartExploding());
        }
        if (collision.gameObject.tag == "PlayerAmmo")
        {
            StartCoroutine(HittingMissle());
        }
        if (collision.gameObject.tag == "PlayerRocket")
        {
            StartCoroutine(StartExploding());
        }
    }
}
