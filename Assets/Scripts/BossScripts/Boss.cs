﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    [SerializeField]
    protected GameObject misslePrefab;

    [SerializeField]
    private int numberOfBombs;

    [HideInInspector]
    public int numberOfMissles = 1;

    [SerializeField]
    public Stat healthStat;

    [SerializeField]
    private Canvas bossHealthBarCanvas;

    [SerializeField]
    private GameObject bombPrefab;

    [HideInInspector]
    public int MaxHealth;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Mode"))
        {
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                numShots = 1;
                numberOfBombs = 5;
                numberOfMissles = 1;
            }
            else
            {
                numShots = 4;
                numberOfBombs = 10;
                numberOfMissles = 4;
            }
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        MaxHealth = health;
        point = 10000;
        bullets = new GameObject[(int)numShots];
        healthStat.MaxValue = health;
        healthStat.CurrentValue = health;
        TankController.Tank.DeadEventHandler += new EventHandler(RemovingTarget);
        ChangeState(new BossIdleState());
    }

    public override bool IsDead
    {
        get
        {
            return healthStat.CurrentValue <= 0;
        }
    }


    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Edge"))
        {
            ChangeDirection();
        }
        if (damageSources.Contains(collision.tag))
        {
            if (!IsDead)
            {
                StartCoroutine(TakingDamage());
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Target == null)
        {
            if (GameObject.Find("Tank") != null)
            {
                Target = GameObject.FindGameObjectWithTag("Player");
            }
        }
        if (!IsDead)
        {
            if (!Target.GetComponent<TankController>().IsDead)
            {
                currentState.Execute();
            }
            else
            {
                ChangeState(new BossIdleState());
                currentState.Execute();
            }
        }
    }

    public override IEnumerator InitializeBullets()
    {
        if (Target != null)
        {
            float xDir = Target.transform.position.x - transform.position.x;
            float yDir = Target.transform.position.y - transform.position.y;
            for (int i = 0; i < bullets.Length; i++)
            {
                bullets[i] = ObjectPooler.SharedInstance.GetPooledObject("EnemyAmmo");
                bullets[i].transform.position = shootingPos.position;
                bullets[i].transform.rotation = Quaternion.identity;
                bullets[i].GetComponent<EnemyBullet>().Initialize(new Vector3(xDir, yDir, 0));
                bullets[i].SetActive(true);
                yield return new WaitForSeconds(.2f);
            }
        }
    }


    public IEnumerator InitializeMissles()
    {
        if (Target != null)
        {
            for (int i = 0; i < numberOfMissles; i++)
            {
                var missle = ObjectPooler.SharedInstance.GetPooledObject("EnemyMissle");

                missle.transform.position = shootingPos.position;
                missle.transform.rotation = Quaternion.identity;
                missle.SetActive(true);
                if (!facingRight)
                {
                    missle.GetComponent<HomingMissle>().ChangeDirection();
                }
                yield return new WaitForSeconds(.2f);
            }
        }
    }


    public IEnumerator InitializeBomb()
    {
        if (bombPrefab != null)
        {
            for (int i = 0; i < numberOfBombs; i++)
            {
                var bomb = ObjectPooler.SharedInstance.GetPooledObject("EnemyBomb");
                bomb.transform.position = transform.Find("dropPos").position;
                bomb.SetActive(true);
                yield return new WaitForSeconds(.2f);
            }
        }
    }


    public override IEnumerator IndicatingDeath()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }

    public override IEnumerator TakingDamageWithLaser(int laserDamage)
    {
        healthStat.CurrentValue -= laserDamage;
        health -= laserDamage;
        if (Time.deltaTime >= 0.2f)
        {
            healthStat.CurrentValue -= laserDamage * 2;
        }
        if (!bossHealthBarCanvas.isActiveAndEnabled)
        {
            bossHealthBarCanvas.gameObject.SetActive(true);
        }
        if (!IsDead)
        {
            StartCoroutine(IndicatingTakingDamage());
        }
        else
        {
            TankController.Tank.tank.Point += point;
            if (this is Enemy)
            {
                UponDeath();
            }
            var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
            clone.transform.position = transform.position;
            clone.GetComponent<FloatingPoint>().thingToFloat = point.ToString();
            MyAnimator.SetTrigger("dead");
            StartCoroutine(IndicatingDeath());
        }
        yield return null;
    }

    public override IEnumerator TakingDamage()
    {
        if (TankController.Tank.tank.RocketDamage > 0)
        {
            healthStat.CurrentValue -= TankController.Tank.tank.RocketDamage;
        }
        health -= TankController.Tank.tank.DealingDamage;
        healthStat.CurrentValue -= TankController.Tank.tank.DealingDamage;
        if (!bossHealthBarCanvas.isActiveAndEnabled)
        {
            bossHealthBarCanvas.gameObject.SetActive(true);
        }
        if (!IsDead)
        {
            StartCoroutine(IndicatingTakingDamage());
        }
        else
        {
            TankController.Tank.tank.Point += point;
            if (this is Enemy)
            {
                UponDeath();
            }
            MyAnimator.SetTrigger("dead");
            bossHealthBarCanvas.gameObject.SetActive(false);
            StartCoroutine(IndicatingDeath());
        }
        yield return null;
    }

    public void ShootingMissle()
    {
        StartCoroutine(InitializeMissles());
    }

    public void ExcutingSpecialState()
    {
        MyAnimator.SetTrigger("special");
    }

    public override void OnDeath()
    {
        base.OnDeath();
    }

}
