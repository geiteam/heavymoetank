﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdleState : IEnemyState
{
    private Boss boss;

    private float idleTimer;

    private float idleDuration = 1f;

    public void Enter(Enemy enemy)
    {
        this.boss = (Boss)enemy;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        idleTimer += Time.deltaTime;
        Idling();
    }

    public void Exit()
    {
    }

    private void Idling()
    {
        boss.MyAnimator.SetFloat("flying", 0);
        if (idleTimer >= idleDuration)
        {
            if (boss.Target != null)
            {
                boss.ChangeState(new BossAttackState());
            }
        }
    }
}
