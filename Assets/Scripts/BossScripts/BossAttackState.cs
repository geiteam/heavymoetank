﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackState : IEnemyState {

    private Boss boss;

    private float attackTimer;

    private float attackDuration = 2.5f;

    private void Attacking()
    {
        if(attackTimer >= attackDuration)
        {
            boss.ChangeState(new BossPatrolState());
        }
    }

    public void Enter(Enemy enemy)
    {
        boss = (Boss)enemy;
        float xDir = boss.Target.transform.position.x - boss.transform.position.x;
        if (xDir < 0 && boss.facingRight || xDir > 0 && !boss.facingRight)
        {
            boss.ChangeDirection();
        }
        boss.MyAnimator.SetTrigger("attack");
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        attackTimer += Time.deltaTime;
        Attacking();
    }

    public void Exit()
    {
    }

}
