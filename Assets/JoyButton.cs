﻿using UnityEngine.EventSystems;
using UnityEngine;

public class JoyButton : MonoBehaviour,IPointerUpHandler,IPointerDownHandler {

    public bool pressed;

    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
